package com.example.demo;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * @author dchandler
 */
@RestController
public class GreetingController {

  @Value("${demo.greeting:local}")
  private String greeting;

  @RequestMapping("/greeting")
  public String greeting() {
    return greeting;
  }
}