# Containerise the application

## Run locally

`docker run -p 8080:8080 demo:0.0.1-SNAPSHOT`

## Deploy to Kubernetes with Kustomize

This is a sample web application that uses Kustomize to deploy to different environments (production and staging).

### Deploy to MicroK8s

To deploy, run the following:

`kubectl apply -k k8s/kustomize/overlays/development`

`kubectl apply -k k8s/kustomize/overlays/staging`

`kubectl apply -k k8s/kustomize/overlays/production`

```
docker build . -t 192.168.56.102:32000/platform-demo:latest
docker push 192.168.56.102:32000/platform-demo
```

If you get this error: `Get https://192.168.56.102:32000/v2/: http: server gave HTTP response to HTTPS client`

```
/etc/docker/daemon.json:

{
  "insecure-registries" : ["192.168.15.102:32000"]
}
```

To apply these Resources, run kubectl apply with --kustomize or -k flag:

```
kubectl apply -k k8s/kustomize/overlays/development/
kubectl port-forward development-the-deployment-86bf99b58f-mgrs6 8080:8080 --address 192.168.56.101
```
